﻿Shader "Custom/AdditiveRuneCircle" {
	Properties{
		_TintColor("Tint Color", Color) = (0.5,0.5,0.5,0.5)
		_MainTex("Particle Texture", 2D) = "white" {}
		_InvFade("Soft Particles Factor", Range(0.01,3.0)) = 1.0
		_RotationSpeed("Rotation Speed", Range(0.01, 10)) = 1.0
		_PulseSpeed("Alpha Pulse Speed", Range(25, 100)) = 1.0
	}

	Category{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend SrcAlpha One
		AlphaTest Greater .01
		ColorMask RGB
		Cull Off Lighting Off ZWrite Off Fog{ Color(0,0,0,0) }
		BindChannels{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
		
			// ---- Fragment program cards		
			SubShader{			
			Pass{
				
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#pragma multi_compile_particles

#include "UnityCG.cginc"

		sampler2D _MainTex;	
		fixed4 _TintColor;
		float _RotationSpeed;

		struct appdata_t {
			float4 vertex : POSITION;
			fixed4 color : COLOR;
			float2 texcoord : TEXCOORD0;
		};

	
		struct v2f {
			float4 vertex : POSITION;
			fixed4 color : COLOR;
			float2 texcoord : TEXCOORD0;
#ifdef SOFTPARTICLES_ON
			float4 projPos : TEXCOORD1;
#endif
		};

		float4 _MainTex_ST;

		v2f vert(appdata_t v)
		{
			v2f o;
			o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
#ifdef SOFTPARTICLES_ON
			o.projPos = ComputeScreenPos(o.vertex);
			COMPUTE_EYEDEPTH(o.projPos.z);
#endif
			o.color = v.color;
			o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);

			// rotate around center
			float s = sin(_RotationSpeed * _Time);
			float c = cos(_RotationSpeed * _Time);
			float2x2 rotationMatrix = float2x2(c, -s, s, c);
			rotationMatrix *= 0.5;
			rotationMatrix += 0.5;
			rotationMatrix = rotationMatrix * 2 - 1;
			o.texcoord.xy += mul(v.texcoord.xy - 0.5, rotationMatrix);
			o.texcoord.xy += .5;
			//

			return o;
		}

		sampler2D _CameraDepthTexture;
		float _InvFade;
		float _PulseSpeed;

		fixed4 frag(v2f i) : COLOR
		{
#ifdef SOFTPARTICLES_ON
				float sceneZ = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos))));
				float partZ = i.projPos.z;
				float fade = saturate(_InvFade * (sceneZ - partZ));
				i.color.a *= fade;
#endif
				// tint color pulse
				_TintColor.a = sin(_Time * _PulseSpeed) * 0.25 + 0.35;

				// decolorize texture
				fixed4 textureColor = tex2D(_MainTex, i.texcoord);
				textureColor = (textureColor.r + textureColor.g + textureColor.b) / 3;

				// cut of everything outside of circle
				textureColor.a = lerp(0, textureColor.a, step(length(i.texcoord.xy - 0.5), 0.5));

				return 2.0f * i.color * _TintColor * textureColor;	
		}
		ENDCG
		}
	}

		// ---- Dual texture cards
		SubShader{
			Pass{
				SetTexture[_MainTex]{
					constantColor[_TintColor]
					combine constant * primary
				}
				SetTexture[_MainTex]{
					combine texture * previous DOUBLE
				}
			}
		}

		// ---- Single texture cards (does not do color tint)
		SubShader{
			Pass{
				SetTexture[_MainTex]{
					combine texture * primary
				}
			}
		}
	}
}
