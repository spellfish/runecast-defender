using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Agent that controls character movement
	/// </summary>
	public interface IMoveAgent
	{
		bool enabled { get; set; }
		Path Path { get; set; }

		Vector3 Velocity { get; }
		Transform transform { get; }
		Transform TargetTransform { get; }
		bool ReachedTarget { get; }
	}
}
