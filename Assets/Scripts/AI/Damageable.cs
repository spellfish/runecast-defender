namespace Spellfish
{
	/// <summary>
	/// Object that can be damaged and healed
	/// </summary>
	public interface IDamageable
	{
		/// <summary>
		/// Current amount of Hitpoints
		/// </summary>
		float HP { get; }

		/// <summary>
		/// Is the object alive? (HP > 0)
		/// </summary>
		bool Alive { get; }

		/// <summary>
		/// Deal a set amount of damage to the object
		/// </summary>
		/// <param name="amount">Damage amount</param>
		void Damage (float amount);

		/// <summary>
		/// Heal the object by a set amount of HP
		/// </summary>
		/// <param name="amount">Heal amount</param>
		void Heal (float amount);
	}
}
