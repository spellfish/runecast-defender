using UnityEngine;
using System.Collections.Generic;
using Valve.VR.InteractionSystem;
using System;
using System.Collections;
using UnityEngine.Events;

namespace Spellfish
{
	/// <summary>
	/// Enemy NPC that attacks a BaseBuilding
	/// </summary>
	public class Enemy : MonoBehaviour, IDamageable, IFireballTarget
	{
		public int maxHP = 1;

		public float HP { get; private set; }
		public bool Alive { get { return HP > 0; } }

		[SerializeField]
		public UnityEvent OnAttack;
		public UnityEvent OnDeath;
		public UnityEvent OnFire;
		private GameObject ShieldAnimGO_Instance;

		public GameObject Fire;

		public int attackDamage = 1;
		public float attackCooldown = 2f;
		float attackTimer;

		[Header("Materials")]
		public Material defaultMaterial;
		public Material selectedMaterial;
		public Material damageMaterial;

		[Header("Audio Clips")]
		public AudioClip die;
		public AudioClip stun;
		public AudioClip burn;
		public AudioClip attack;
		public AudioClip walk;

		public bool isOnFire;

		/// <summary>
		/// Time the enemy needs to recover once it hits the ground
		/// </summary>
		[Tooltip("Time the enemy needs to recover once it hits the ground")]
		public float stunAfterDropDuration = 2f;
		float recoverFromStunTimer = 0f;

		internal IMoveAgent MoveAgent { get { return moveAgent; } }
		IMoveAgent moveAgent;
		AudioSource audioSource;


		public static List<Enemy> _All;
		public static List<Enemy> All
		{
			get { if (_All == null) _All = new List<Enemy>(); return _All; }
			private set { _All = value; }
		}

		public UI.FireballMarker FireballMarker { get; set; }

		BaseBuilding baseBuilding;
		MeshRenderer meshRenderer;
		Rigidbody rigidbody_;
		VelocityEstimator velocityEstimator;
		Animator animator;
		StatBar hpBar;

		public SteamVR_GazeTracker GazeTracker { get; private set; }



		void Awake ()
		{
			All.Add(this);

			baseBuilding = FindObjectOfType<BaseBuilding>();
			moveAgent = GetComponent<IMoveAgent>();
			GazeTracker = GetComponent<SteamVR_GazeTracker>();
			meshRenderer = GetComponent<MeshRenderer>();
			velocityEstimator = GetComponent<VelocityEstimator>();
			rigidbody_ = GetComponent<Rigidbody>();
			animator = GetComponent<Animator>();
			hpBar = GetComponentInChildren<StatBar>(true);
			FireballMarker = GetComponentInChildren<UI.FireballMarker>(true);

			audioSource = gameObject.AddComponent<AudioSource>();
			audioSource.priority = 200;
			audioSource.spatialBlend = 1;
			audioSource.maxDistance = 150;
			audioSource.minDistance = 15;
			audioSource.playOnAwake = false;

			FindObjectOfType<GameGoal>().Defeat += Enemy_Defeat;

			HP = maxHP;
		}

		private void Enemy_Defeat (object sender, EventArgs e)
		{
			animator.SetBool("defeat", true);
			moveAgent.enabled = false;
			enabled = false;
		}

		void Update ()
		{
			if (hpBar != null)
				hpBar.Set(HP, maxHP);

			if (HP <= 0)
				Die();

			if (recoverFromStunTimer > 0)
			{
				recoverFromStunTimer -= Time.deltaTime;
				if (recoverFromStunTimer <= 0)
					RecoverFromDrop();
			}

			if (moveAgent.ReachedTarget && moveAgent.enabled)
				Attack();

			animator.SetFloat("movespeed", moveAgent.Velocity.magnitude);
		}

		private void OnDestroy ()
		{
			FindObjectOfType<GameGoal>().Defeat -= Enemy_Defeat;

			All.Remove(this);
		}

		void OnCollisionEnter (Collision collision)
		{
			if (LayerManager.Instance.groundLayers.IsInLayerMask(collision.gameObject))
				Stun(stunAfterDropDuration);
		}

		/// <summary>
		/// Stun the Enemy for a set amount of time
		/// </summary>
		/// <param name="duration">How long should the Enemy be stunned?</param>
		public void Stun (float duration)
		{
			recoverFromStunTimer = duration;
			animator.SetTrigger("stun");
			moveAgent.enabled = false;
			audioSource.PlayOneShot(stun);
		}

		/// <summary>
		/// Damage the enemy by a set amount of HP
		/// </summary>
		/// <param name="amount">Damage amount</param>
		public void Damage (float amount)
		{
			HP -= amount;
		}

		/// <summary>
		/// Heal the enemy by a set amount of HP
		/// </summary>
		/// <param name="amount">Heal amount</param>
		public void Heal (float amount)
		{
			HP += amount;
			if (HP > maxHP)
				HP = maxHP;
		}

		/// <summary>
		/// Draw enemy with highlight/selection effect
		/// </summary>
		public void Select ()
		{
			meshRenderer.material = selectedMaterial;
		}

		/// <summary>
		/// Remove highlight effect
		/// </summary>
		public void Deselect ()
		{
			meshRenderer.material = defaultMaterial;
		}

		/// <summary>
		/// Disables movement so it can be moved externally
		/// </summary>
		public void Grab ()
		{
			//
			ShieldAnimGO_Instance.SetActive(true);
			//

			moveAgent.enabled = false;
			velocityEstimator.BeginEstimatingVelocity();
			rigidbody_.isKinematic = true;

			animator.SetTrigger("pickup");
		}

		/// <summary>
		/// Drop enemy as Rigidbody. Enemy recovers once it hits the ground
		/// </summary>
		public void Drop ()
		{
			velocityEstimator.FinishEstimatingVelocity();

			//
			ShieldAnimGO_Instance.SetActive(false);
			//
			rigidbody_.isKinematic = false;
			rigidbody_.mass = 10;
			rigidbody_.interpolation = RigidbodyInterpolation.Interpolate;

			var velocity = velocityEstimator.GetVelocityEstimate();

			//
			velocity = Vector3.ClampMagnitude(velocity, 10);
			//

			var angularVelocity = velocityEstimator.GetAngularVelocityEstimate();
			var position = velocityEstimator.transform.position;

			Vector3 r = transform.TransformPoint(rigidbody_.centerOfMass) - position;
			rigidbody_.velocity = velocity + Vector3.Cross(angularVelocity, r);
			rigidbody_.angularVelocity = angularVelocity;

			float timeUntilFixedUpdate = (Time.fixedDeltaTime + Time.fixedTime) - Time.time;
			transform.position += timeUntilFixedUpdate * velocity;
			float angle = Mathf.Rad2Deg * angularVelocity.magnitude;
			Vector3 axis = angularVelocity.normalized;
			transform.rotation *= Quaternion.AngleAxis(angle * timeUntilFixedUpdate, axis);
		}

		public void StopMovement ()
		{
			moveAgent.enabled = false;
			velocityEstimator.BeginEstimatingVelocity();
			rigidbody_.isKinematic = true;
		}

		public void SetOnFire (float TotalDamage, float TotalDuration, float StepDuration)
		{
			if (isOnFire == false)
			{
				isOnFire = true;
				DamageOverTime(TotalDamage, TotalDuration, StepDuration);
				Fire.SetActive(true);
				OnFire.Invoke();
				audioSource.clip = burn;
                audioSource.Play();

			}
		}

		void RecoverFromDrop ()
		{
			//rigidbody has to be set kinematic BEFORE the navmesh-agent is re-enabled
			rigidbody_.isKinematic = false;
			moveAgent.enabled = true;
		}

		public void DamageOverTime (float totalDamage, float totalDuration, float stepDuration)
		{
			StartCoroutine(DamageOverTimeRoutine(totalDamage, totalDuration, stepDuration));
		}


		void Attack ()
		{
			if (attackTimer > 0)
			{
				attackTimer -= Time.deltaTime;
				return;
			}

			attackTimer = attackCooldown;
			baseBuilding.Damage(attackDamage);
			OnAttack.Invoke();
			animator.SetTrigger("attack");
			audioSource.PlayOneShot(attack);
		}

		void Die ()
		{
			enabled = false;
			MoveAgent.enabled = false;
			All.Remove(this);
			OnDeath.Invoke();
			//Debug.Log("Dead _ " + this.name);
			animator.SetTrigger("die");
			audioSource.PlayOneShot(die);
			hpBar.gameObject.SetActive(false);
		}

		public void OnFireballHit (float damage)
		{
			DamageNow(damage);
		}

		void DamageNow (float amount)
		{
			if (HP > 0)
			{
				HP -= amount;

				if (HP <= 0)
					Die();
			}
		}

		bool CRrunning;
		int count;

		IEnumerator DamageOverTimeRoutine (float totalDamage, float totalDuration, float stepDuration)
		{
			count++;
			if (CRrunning)
				yield break;

			CRrunning = true;

			float stepDamagePart = (totalDuration / stepDuration);
			float stepDamage = totalDamage / stepDamagePart;
			var remainingDamage = totalDamage;
			float timer = stepDuration;

			var duration = 0f;

			while (remainingDamage > 0)
			{
				timer -= Time.deltaTime;
				duration += Time.deltaTime;
				if (timer <= 0)
				{
					DamageNow(stepDamage);
					if (HP > 0)
						animator.SetTrigger("hit");

					timer = stepDuration;
					remainingDamage -= stepDamage;
				}
				yield return null;
			}
            isOnFire = false;
			Fire.SetActive(false);
			CRrunning = false;
		}
	}
}
