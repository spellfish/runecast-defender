using System;
using System.Collections;
using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Manages wave-based mob spawning
	/// </summary>
	public class SpawnManager : MonoBehaviour
	{
		[Header("Random Spawning")]
		[Tooltip("Overrides Pattern Spawning")]
		public bool randomSpawningEnabled;
		public int maximumWaveCount;
		public FloatPair spawnIntervalRange;
		public IntPair mobSizeRange;
		public float maxWaveTime;
		public int maxWaveMonsterCount;
		[Tooltip("Maximum number of enemies, that can appear simultaneously")]
		public int maxEnemyCount;

		[Header("Pattern Spawning (default)")]
		/// <summary>
		/// Json file which is read to create a spawnpattern
		/// </summary>
		public TextAsset spawnPatternJson;
		public Pattern spawnPattern;
		/*
		[TextArea(20, 50)]
		public string jsonString;
		*/

		[Header("Shared Settings")]
		public float timeBetweenWaves = 15f;
		public float timeAfterGameStart = 5f;
		public float prewarmDuration = 3f;

		[Header("References")]
		/// <summary>
		/// Enemy prefabs need unique names!
		/// </summary>
		public Enemy[] enemyPrefabs;
		public SpawnPoint[] spawnPoints;


        internal event EventHandler waveEnd;
		internal event EventHandler waveStart;
		internal int currentWave = -1;

		float intervalTimer;
		float waveCountDown;
		float waveTime;
		int waveCount = int.MaxValue;
		Timer waveSpawnTimer;
		int waveSpawnCounter = 0;
		float lastSpawnTime = 0;


		static SpawnManager instance;
		public static SpawnManager Instance
		{
			get
			{
				if (instance == null)
					Debug.LogError("No SpawnManager instance located");

				return instance;
			}
		}


		private void Awake()
		{
			if (instance != null)
				Destroy(this);
			else
				instance = this;

			waveSpawnTimer = GetComponentInChildren<Timer>(true);
			FindObjectOfType<GameGoal>().Defeat += SpawnManager_Defeat;

			if (randomSpawningEnabled == false && spawnPatternJson != null)
				spawnPattern = JsonUtility.FromJson<Pattern>(spawnPatternJson.text);
			//jsonString = JsonUtility.ToJson(spawnPattern, true);

			foreach (Enemy e in enemyPrefabs)
				foreach (Wave w in spawnPattern.waves)
					foreach (Batch b in w.spawns)
						if (b.enemy == e.name)
							b.EnemyPrefab = e;

			waveCount = randomSpawningEnabled ? maximumWaveCount : spawnPattern.waves.Length;
			waveCountDown = timeAfterGameStart;
		}

		private void SpawnManager_Defeat(object sender, EventArgs e)
		{
			gameObject.SetActive(false);
		}

		private void Update()
		{
			if (waveCountDown > 0)
			{
				waveSpawnTimer.Show();
				waveCountDown -= Time.deltaTime;

				if (currentWave == -1)
					waveSpawnTimer.SetTime(waveCountDown, "Game starts in");
				else if (GetRemainingWaves() > 0)
					waveSpawnTimer.SetTime(waveCountDown, "Wave " + (currentWave + 2) + " starts in");

				if (waveCountDown <= 0)
				{
					waveSpawnTimer.Hide();

					if (currentWave >= waveCount)
						Debug.Log("No more waves");
					else
					{
						currentWave++;
						Debug.Log("Wave " + (currentWave) + " started");
						waveTime = 0;
						waveSpawnCounter = 0;
						lastSpawnTime = spawnPattern.waves[currentWave].spawns[spawnPattern.waves[currentWave].spawns.Length - 1].time + prewarmDuration + 1;
						if (waveStart != null)
							waveStart(this, EventArgs.Empty);
					}
				}

				return;
			}

			waveTime += Time.deltaTime;

			if (randomSpawningEnabled)
			{
				SpawnRandom();
			}
			else
			{
				SpawnPattern();
			}

			if (GetCurrentWaveOver())
				FinishWave();
		}

		public bool GetCurrentWaveOver()
		{
			return Enemy.All.Count == 0 && waveTime > lastSpawnTime && GetRemainingWaves() >= 0;
		}

		public int GetRemainingWaves()
		{
			return waveCount - (currentWave + 1);
		}


		void FinishWave()
		{
			if (waveEnd != null)
				waveEnd(this, EventArgs.Empty);

			if (GetRemainingWaves() > 0)
				waveCountDown = timeBetweenWaves;
		}

		void SpawnRandom()
		{
			intervalTimer -= Time.deltaTime;

			if (Enemy.All.Count >= maxEnemyCount || waveTime >= maxWaveTime || waveSpawnCounter >= maxWaveMonsterCount)
				return;

			if (intervalTimer <= 0)
			{
				intervalTimer = Random.Range(spawnIntervalRange);

				var count = Random.Range(mobSizeRange);
				var spawnPoint = Random.Of(spawnPoints);
				var enemyPrefab = Random.Of(enemyPrefabs);

				spawnPoint.Spawn(count, enemyPrefab);

				waveSpawnCounter += count;
			}
		}

		void SpawnPattern()
		{
			if (currentWave < waveCount)
				foreach (Batch b in spawnPattern.waves[currentWave].spawns)
					if (b.time < waveTime && b.Spawned == false)
					{
						StartCoroutine(SpawnEnumerator(b));
						b.Spawned = true;
					}
		}

		IEnumerator SpawnEnumerator(Batch b)
		{
            // Insert prewarm effect here
            spawnPoints[b.spawnPoint].PrewarmEffect.GetComponent<ParticleSystem>().Play();
			yield return new WaitForSeconds(prewarmDuration);
			spawnPoints[b.spawnPoint].Spawn(b.count, b.EnemyPrefab);
			yield break;
		}


		[Serializable]
		public class Pattern
		{
			public Wave[] waves;

			public Pattern()
			{
				waves = new Wave[2];

				for (int i = 0; i < waves.Length; i++)
					waves[i] = new Wave();
			}
		}

		[Serializable]
		public class Wave
		{
			public Batch[] spawns;

			public Wave()
			{
				spawns = new Batch[3];

				for (int i = 0; i < spawns.Length; i++)
					spawns[i] = new Batch();
			}
		}

		[Serializable]
		public class Batch
		{
			public int spawnPoint;
			public int time;
			public Enemy EnemyPrefab { get; set; }
			public string enemy;
			public int count;

			public bool Spawned;

			public Batch() { }
			public Batch(int spawnPoint, int time, Enemy EnemyPrefab, string enemy, int count, bool Spawned)
			{
				this.spawnPoint = spawnPoint;
				this.time = time;
				this.EnemyPrefab = EnemyPrefab;
				this.enemy = enemy;
				this.count = count;
			}
		}
	}
}
