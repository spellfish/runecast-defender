using UnityEngine;
using UnityEngine.Events;


namespace Spellfish
{
	/// <summary>
	/// Spawns Enemy objects
	/// </summary>
	public class SpawnPoint : MonoBehaviour
	{
		/// <summary>
		/// Spawned enemies follow this path. It's reccomended to place the path component on the same GameObject.
		/// </summary>
		public Path path;
		public UnityEvent MobSpawned;
        private AudioSource audiosource;
        public AudioClip spawn;
        public GameObject PrewarmEffect;


        private void Awake()
        {
            audiosource = gameObject.AddComponent<AudioSource>();
            audiosource.volume = 0.5f;
            audiosource.priority = 75;
            audiosource.spatialBlend = 1;
            audiosource.maxDistance = 150;
            audiosource.minDistance = 15;
            audiosource.playOnAwake = false;
        }
        
		public void Spawn (int count, Enemy enemyPrefab)
		{
			for (int i = 0; i < count; i++)
			{
				Vector3 targetPosition = transform.position + new Vector3(Random.Range(-.1f, .1f), Random.Range(-.1f, .1f), Random.Range(-.1f, .1f));
				var _new = Instantiate(enemyPrefab, targetPosition, new Quaternion());
				_new.gameObject.SetActive(true);
				_new.MoveAgent.Path = path;
				MobSpawned.Invoke();
                audiosource.PlayOneShot(spawn);
            }

		}
	}
}
