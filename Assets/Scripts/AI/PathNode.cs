using UnityEngine;

namespace Spellfish
{
	public class PathNode : MonoBehaviour
	{
		public Path path;
		private void OnDrawGizmosSelected()
		{
			if (path != null)
			{
				Gizmos.color = path.color;
				Gizmos.DrawSphere(transform.position, path.nodeRadius);
			}
		}
	}
}
