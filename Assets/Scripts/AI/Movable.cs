using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Describes an object that is defined by its Velocity and Transform
	/// </summary>
	public interface IMovable
	{
		Vector3 Velocity { get; set; }

		/// <summary>
		/// Equivalent to Unity's transform property.
		/// (Which is also why it's written lowercase)
		/// </summary>
		Transform transform { get; }
	}
}
