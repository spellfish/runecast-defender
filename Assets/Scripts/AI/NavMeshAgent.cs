using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Spellfish
{
	/// <summary>
	/// Custom wrapper for Unity.AI.NavMeshAgent
	/// </summary>
	[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
	public class NavMeshAgent : MonoBehaviour, IMoveAgent
	{
		public float recoverRange = 5f;

		public bool Active { get { return enabled; } set { enabled = value; unityAgent.enabled = value; } }
		public Path Path { get; set; }

		public Vector3 Velocity { get { return unityAgent.velocity; } }

		public Transform TargetTransform
		{
			get
			{
				if (Path != null)
					return Path.nodes[currentPathNode];
				else
					return transform;
			}
			private set
			{
				unityAgent.destination = value.position;
			}
		}
		public bool ReachedTarget { get; private set; }

		int currentPathNode;
		UnityEngine.AI.NavMeshAgent unityAgent;


		void Awake ()
		{
			unityAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		}

		void Update ()
		{
			unityAgent.SetDestination(TargetTransform.position);

			var _distanceToNextTarget = Vector3.Distance(transform.position, TargetTransform.position);

			if (Path != null)
				if (_distanceToNextTarget <= Path.nodeRadius)
				{
					if (currentPathNode < Path.nodes.Length - 1)
						currentPathNode++;
					else
						ReachedTarget = true;
				}
		}

		private void OnEnable ()
		{
			GetComponent<Rigidbody>().isKinematic = true;
			NavMeshHit myNavHit;
			if (NavMesh.SamplePosition(transform.position, out myNavHit, recoverRange, -1))
			{
				unityAgent.enabled = true;
				unityAgent.Warp(myNavHit.position);
			}
			else
			{
				Active = false;
			}
		}

		private void OnDisable ()
		{
			unityAgent.enabled = false;
		}
	}
}
