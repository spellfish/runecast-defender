using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Movement path that ends with a BaseBuilding.
	/// The path is automatically generated from PathNode components on child objects.
	/// </summary>
	public class Path : MonoBehaviour
	{
		/// <summary>
		/// Radius of path nodes.
		/// This is used by certain NPC movement scripts.
		/// </summary>
		public float nodeRadius = 2f;
		public Color color;

		internal Transform[] nodes;

		void Awake ()
		{
			var _elements = GetComponentsInChildren<PathNode>();
			nodes = new Transform[_elements.Length + 1];
			for (int i = 0; i < _elements.Length; i++)
			{
				nodes[i] = _elements[i].transform;
			}

			var baseBuilding = FindObjectOfType<BaseBuilding>();
			if (baseBuilding != null)
				nodes[_elements.Length] = baseBuilding.transform;
		}
	}
}
