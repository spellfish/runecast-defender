using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Destroys the topmost parent if particle system is not playing
	/// </summary>
	[RequireComponent(typeof(ParticleSystem))]
	public class ParticleSystemReactor : MonoBehaviour
	{
		ParticleSystem particleSys;

		private void Awake ()
		{
			particleSys = GetComponent<ParticleSystem>();
		}

		private void Update ()
		{
			if (particleSys.isPlaying == false)
				Destroy(transform.root.gameObject);
		}
	}
}
