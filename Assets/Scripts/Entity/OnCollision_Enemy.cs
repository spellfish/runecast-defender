using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using Spellfish;

namespace Spellfish
{

	public class OnCollision_Enemy : MonoBehaviour
	{
		[SerializeField]
		public UnityEvent OnCollision;
		public int TotalDamage = 250;
		private float StepDuration = 1f;
		public float TotalDuration = 5;


        private SphereCollider sphereCollider;
		private BoxCollider boxCollider;

		private bool IsBoxCollider = false;

		public bool SetOnFire;
        public bool DamageContinuous;

		private void Awake ()
		{
			sphereCollider = GetComponent<SphereCollider>();
			boxCollider = GetComponent<BoxCollider>();
			StepDuration = TotalDuration / 5;
			if (boxCollider != null)
				IsBoxCollider = true;
		}

		private void Update ()
		{
			if (IsBoxCollider)
			{
				foreach (Enemy e in Enemy.All)
					if (boxCollider.bounds.Contains(e.transform.position))
					{
                        if (SetOnFire)
							if (e.isOnFire == false)
							{
								e.SetOnFire(TotalDamage, TotalDuration, StepDuration);
								e.isOnFire = true;
							}
						if (DamageContinuous)
						{
							e.SetOnFire(TotalDamage, float.Epsilon, float.Epsilon);
						}
						OnCollision.Invoke();
					}
			}
			else
			{
				foreach (Enemy e in Enemy.All)
					if (Vector3.Distance(e.transform.position, transform.position) <= sphereCollider.radius)
					{
						if (SetOnFire)
							if (!e.isOnFire)
							{
								e.SetOnFire(TotalDamage, TotalDuration, StepDuration);
							}
						OnCollision.Invoke();
					}
				return;

			}
		}
	}
}
