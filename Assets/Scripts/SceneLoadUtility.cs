using UnityEngine;
using UnityEngine.SceneManagement;

namespace Spellfish
{

	public class SceneLoadUtility : MonoBehaviour
	{
		public GameObject[] objectsToDeactivateOnLoad;
		public MonoBehaviour[] behavioursToDisableOnLoad;

		[Scene]
		public string[] scenesToLoad;

		private void Awake ()
		{
			foreach (var gameObject in objectsToDeactivateOnLoad)
				gameObject.SetActive(false);

			foreach (var behaviour in behavioursToDisableOnLoad)
				behaviour.enabled = false;

			if (scenesToLoad.Length > 0)
				foreach (var scene in scenesToLoad)
					SceneManager.LoadScene(scene, LoadSceneMode.Additive);

			Destroy(gameObject);
		}
	}
}
