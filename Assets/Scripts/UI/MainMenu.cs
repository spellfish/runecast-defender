using UnityEngine;
using System;

namespace Spellfish
{
	/// <summary>
	/// Game Menu / Title Screen
	/// </summary>
	public class MainMenu : MonoBehaviour
	{
		public event EventHandler GameStart;

		static MainMenu instance;
		public static MainMenu Instance
		{
			get
			{
				if (instance == null)
					Debug.LogError("No MainMenu instance located");

				return instance;
			}
		}


		private void Awake()
		{
			instance = this;
		}

		public void StartGame ()
		{
			if(GameStart != null)
				GameStart(this, EventArgs.Empty);

			transform.GetChild(0).gameObject.SetActive(false);

			SpawnManager.Instance.gameObject.SetActive(true);
			SpellManager.instance.EnableSpells();
			BaseBuilding.Instance.enabled = true;
			GameGoal.Instance.enabled = true;
			foreach (var spawnPoint in CrystalSpawnPoint.AllSpawns)
				spawnPoint.enabled = true;
		}
	}
}
