using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	[RequireComponent(typeof(Interactable))]
	public class InteractableHoverAndButtonEvents : MonoBehaviour
	{
		public UnityEvent onHandHoverBegin;
		public UnityEvent onHandHoverEnd;
		public UnityEvent onHoverAndTriggerDown;
		public UnityEvent onHoverAndTriggerUp;

		private void OnHandHoverBegin ()
		{
			onHandHoverBegin.Invoke();
		}

		private void OnHandHoverEnd ()
		{
			onHandHoverEnd.Invoke();
		}

		private void HandHoverUpdate (Hand hand)
		{
			if (hand.controller.GetHairTriggerDown())
				onHoverAndTriggerDown.Invoke();
			else if (hand.controller.GetHairTriggerUp())
				onHoverAndTriggerUp.Invoke();
		}
	}
}
