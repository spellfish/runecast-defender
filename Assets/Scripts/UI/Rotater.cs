using UnityEngine;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	/// <summary>
	/// Rotates the attached transform to face a given transform
	/// </summary>
	public class Rotater : MonoBehaviour
	{
		[Header("Rotate towards a target")]
		public bool targetHmdTransform = true;
		public Transform targetTransform;
		public Vector3 rotationScale = Vector3.one;

		[Header("Rotate over time")]
		public Vector3 localRotationPerSecond;
		Vector3 localRotationEuler = Vector3.zero;

		void Update ()
		{
			transform.rotation = Quaternion.Euler(0, 0, 0);

			if(targetTransform != null)
				RotateTowards();

			if (localRotationPerSecond != Vector3.zero)
				RotateLocally();
		}

		void RotateTowards ()
		{
			var _rotation = new Quaternion();
			_rotation.SetLookRotation(transform.position - targetTransform.position);
			var _euler = _rotation.eulerAngles;
			_euler.x *= rotationScale.x;
			_euler.y *= rotationScale.y;
			_euler.z *= rotationScale.z;
			transform.rotation = Quaternion.Euler(_euler);
		}

		void RotateLocally ()
		{
			var delta = localRotationPerSecond;
			delta.Clamp(0f, Time.deltaTime);
			localRotationEuler += delta;
			localRotationEuler.x -= localRotationEuler.x > 360 ? 360 : 0;
			localRotationEuler.y -= localRotationEuler.y > 360 ? 360 : 0;
			localRotationEuler.z -= localRotationEuler.z > 360 ? 360 : 0;
			localRotationEuler.x += localRotationEuler.x < 360 ? 360 : 0;
			localRotationEuler.y += localRotationEuler.y < 360 ? 360 : 0;
			localRotationEuler.z += localRotationEuler.z < 360 ? 360 : 0;
			transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + localRotationEuler);
		}

		private void OnValidate ()
		{
			if (targetHmdTransform)
				targetTransform = Player.instance.hmdTransform;

			localRotationEuler = Vector3.zero;
		}
	}
}
