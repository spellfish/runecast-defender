using UnityEngine;
using UnityEngine.UI;

namespace Spellfish
{
	/// <summary>
	/// Manages UI representation of HP, Mana etc.
	/// </summary>
	public class StatBar : MonoBehaviour
	{
		/// <summary>
		/// UI Image Component that is filled according to the current percentage
		/// </summary>
		public Image bar;
		/// <summary>
		/// Text component that displays the numerical value
		/// </summary>
		public Text textComponent;


		/// <summary>
		/// Set the bar to a different absolute value.
		/// </summary>
		/// <param name="value">Current value</param>
		/// <param name="maxValue">Maximum possible value (to show the percentage of)</param>
		/// <param name="label">String that should be printed on the attached text field</param>
		public void Set (float value, float maxValue, string label = null)
		{
			gameObject.SetActive(true);
			bar.fillAmount = value / maxValue;

			if (textComponent != null)
				textComponent.text = label != null ? label : value + " / " + maxValue;
		}
	}
}
