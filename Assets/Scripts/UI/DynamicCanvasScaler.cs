using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	/// <summary>
	/// Dynamically scales a canvas, depending on distance to player
	/// </summary>
	[RequireComponent(typeof(Canvas))]
	public class DynamicCanvasScaler : MonoBehaviour
	{
		RectTransform rectTransform;
		Player player;

		void Awake ()
		{
			rectTransform = GetComponent<RectTransform>();
			player = FindObjectOfType<Player>();
		}

		void Update ()
		{
			var distance = Vector3.Distance(player.transform.position, transform.position);
			rectTransform.localScale = rectTransform.localScale.Set(1f + distance / 10);
		}
	}
}
