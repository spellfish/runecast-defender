using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	/// <summary>
	/// Makes button hints appear at the start of the game. Disabling is dealt with in the TutorialManager
	/// </summary>
	public class TutorialHandHints : MonoBehaviour
	{
		Hand hand;
		float timer = 0;

		private void Awake()
		{
			hand = GetComponent<Hand>();
		}

		void Update()
		{
			if (hand != null && hand.controller != null)
			{
				timer += Time.deltaTime;

				if (timer >= 3f)
				{
					ControllerButtonHints.SetHapticTicks(hand, false);
					ControllerButtonHints.ShowButtonHint(hand, EVRButtonId.k_EButton_Grip);
					ControllerButtonHints.ShowTextHint(hand, EVRButtonId.k_EButton_Grip, "Fireball");

					ControllerButtonHints.ShowButtonHint(hand, EVRButtonId.k_EButton_SteamVR_Trigger);
					ControllerButtonHints.ShowTextHint(hand, EVRButtonId.k_EButton_SteamVR_Trigger, "Telekinesis");

					Destroy(this);
				}
			}
		}
	}
}
