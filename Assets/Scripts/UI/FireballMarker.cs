using UnityEngine;

namespace Spellfish.UI
{
	/// <summary>
	/// Highlight for IFireballTargets
	/// </summary>
	public class FireballMarker : MonoBehaviour
	{
		public void Show ()
		{
			gameObject.SetActive(true);
		}

		public void Hide ()
		{
			gameObject.SetActive(false);
		}
	}
}
