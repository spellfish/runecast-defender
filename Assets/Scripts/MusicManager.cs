using UnityEngine;

namespace Spellfish
{

	public class MusicManager : MonoBehaviour
	{
		public static MusicManager Instance { get; private set; }

		public AudioClip startScreenMusic, combatMusic, victoryMusic, defeatMusic;

		AudioSource audioSource;

		private void Start()
		{
			if (Instance != null)
				Destroy(this);
			else
				Instance = this;

			audioSource = gameObject.AddComponent<AudioSource>();
			audioSource.clip = startScreenMusic;
			audioSource.loop = true;
			audioSource.Play();

			GameGoal.Instance.Defeat += OnDefeat;
			GameGoal.Instance.Victory += OnVictory;
			MainMenu.Instance.GameStart += OnGameStart;
		}

		private void OnGameStart(object sender, System.EventArgs e)
		{
			audioSource.clip = combatMusic;
			audioSource.Play();
		}

		private void OnVictory(object sender, System.EventArgs e)
		{
			audioSource.clip = victoryMusic;
			audioSource.Play();
		}

		private void OnDefeat(object sender, System.EventArgs e)
		{
			audioSource.clip = defeatMusic;
			audioSource.Play();
		}
	}
}
