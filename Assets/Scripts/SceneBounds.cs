using UnityEngine;

namespace Spellfish
{
	public class SceneBounds : MonoBehaviour
	{
		public static SceneBounds instance;
		public static SceneBounds Instance
		{
			get
			{
				if (instance == null)
					Debug.LogError("No SceneBounds instance located");

				return instance;
			}
		}

		public Bounds bounds;

		private void Awake ()
		{
			if (instance != null)
				Destroy(this);
			else
				instance = this;
		}

		private void OnDrawGizmosSelected ()
		{
			Gizmos.DrawWireCube(bounds.center, bounds.size);
		}
	}
}
