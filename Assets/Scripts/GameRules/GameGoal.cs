using System;
using UnityEngine;
using UnityEngine.UI;

namespace Spellfish
{
	/// <summary>
	/// Manges the wind and lose conditions of the game
	/// </summary>
	public class GameGoal : MonoBehaviour
	{
		public static GameGoal Instance { get; private set; }

		public Text victoryText;
		public Text defeatText;

		public event EventHandler Victory;
		public event EventHandler Defeat;

		public bool Defeated { get; private set; }
		public bool Won { get; private set; }

        private void Awake ()
		{
			Instance = this;

			Defeated = false;
			Won = false;

			Defeat += GameGoal_Defeat;
			Victory += GameGoal_Victory;
		}

		void Update ()
		{
			if (Defeated || Won)
				return;

			if (IsDefeat())
                Defeat(this, EventArgs.Empty);
			else if (IsVictory())
                Victory(this, EventArgs.Empty);
        }


		bool IsVictory ()
		{
			return SpawnManager.Instance.GetCurrentWaveOver() && SpawnManager.Instance.GetRemainingWaves() <= 0;
		}

		bool IsDefeat ()
		{
			return (BaseBuilding.Instance.Alive == false);
		}


		private void GameGoal_Defeat (object sender, EventArgs e)
		{
			Defeated = true;
			Debug.Log("Defeat!");
			defeatText.enabled = true;
		}

		private void GameGoal_Victory (object sender, EventArgs e)
		{
			Won = true;
			Debug.Log("Victory!");
			victoryText.enabled = true;
		}
	}
}
