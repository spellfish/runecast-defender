using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// A base building that attracts Enemies who try to destroy it
	/// </summary>
	public class BaseBuilding : MonoBehaviour, IDamageable
	{
		public static BaseBuilding Instance { get; private set; }

		public int maxHp = 200;
		public float HP { get; private set; }

		public bool Alive { get { return HP > 0; } }
		StatBar healthBar;


		void Awake()
		{
			if (Instance != null)
				Destroy(this);
			else
				Instance = this;

			HP = maxHp;
			healthBar = GetComponentInChildren<StatBar>(true);
			enabled = false;
		}

		void Update()
		{
			healthBar.Set(HP, maxHp);
		}

		/// <summary>
		/// Deal a set amount of damage to the Base Building
		/// </summary>
		/// <param name="amount">Damage amount</param>
		public void Damage (float amount)
		{
			if (!Alive)
				return;

			HP -= amount;
			if (HP <= 0)
				HP = 0;
		}

		/// <summary>
		/// Heal the Base Building by a set amount of HP
		/// </summary>
		/// <param name="amount">Heal amount</param>
		public void Heal (float amount)
		{
			if (!Alive)
				return;

			HP += amount;
			if (HP > maxHp)
				HP = maxHp;
		}
	}
}
