using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	/// <summary>
	/// (De)activates tutorial messages
	/// </summary>
	public class TutorialManager : MonoBehaviour
	{
		public GameObject[] waveHints;
		public CrystalSocket[] wave0sockets;

		void Awake()
		{
			SpawnManager.Instance.waveStart += Instance_waveStart;
			SpawnManager.Instance.waveEnd += Instance_waveEnd;
		}

		private void Instance_waveStart(object sender, System.EventArgs e)
		{
			var curWave = SpawnManager.Instance.currentWave;
			if (curWave < waveHints.Length && waveHints[curWave] != null)
				waveHints[curWave].SetActive(true);

			if(curWave == 0)
			{
				foreach (var socket in wave0sockets)
					socket.Activated = true;
			}
		}

		private void Instance_waveEnd(object sender, System.EventArgs e)
		{
			var curWave = SpawnManager.Instance.currentWave;
			if (curWave < waveHints.Length && waveHints[curWave] != null)
				waveHints[curWave].SetActive(false);

			if (curWave == 0)
			{
				foreach (var socket in CrystalSocket.AllSockets)
					socket.Activated = true;

				foreach (var hand in Player.instance.hands)
				{
					if (hand != null && hand.controller != null)
					{
						ControllerButtonHints.HideAllButtonHints(hand);
						ControllerButtonHints.HideAllTextHints(hand);
					}
				}
			}
		}
	}
}
