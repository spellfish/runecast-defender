using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	public class FireballProjectile : MonoBehaviour
	{
		public ParticleSystem fireParticleSystem;
		public ParticleSystem hitParticleSystem;
		public UnityEvent OnHit;
		public event EventHandler Hit;

		float damage;
		internal float speed = 1f;
		IFireballTarget target;
		VelocityEstimator velocityEstimator;
		Animator animator;

		void Awake ()
		{
			velocityEstimator = GetComponent<VelocityEstimator>();
			animator = GetComponent<Animator>();
			enabled = false;
		}

		void Update ()
		{
			if (target != null)
			{
				speed += speed * Time.deltaTime;
				speed += speed * Time.deltaTime;
				speed += speed * Time.deltaTime;
				var delta = speed * Time.deltaTime;

				transform.position = Vector3.MoveTowards(transform.position, target.transform.position, delta);

				if (Vector3.Distance(transform.position, target.transform.position) < 0.1f)
				{
					target.OnFireballHit(damage);
					animator.SetTrigger("hit");
					Dissolve();
					target = null;
					OnHit.Invoke();
					if (Hit != null)
						Hit(this, EventArgs.Empty);
				}
			}
		}


		public void StartFlying (IFireballTarget target, Hand hand, float damage)
		{
			// Uncommenting the following line makes the fireball go in a straight line instead of a spiral
			//animator.SetTrigger("fly");

			this.damage = damage;
			this.hand = hand;
			this.target = target;

			transform.parent = null;
			enabled = true;

			if (target == null)
				Dissolve();
		}

		Hand hand;
		bool dissolving = false;

		public void Dissolve ()
		{
			target = null;
			transform.parent = null;
			velocityEstimator.FinishEstimatingVelocity();

			var rigidbody = GetComponent<Rigidbody>();
			if (rigidbody != null)
			{
				rigidbody.isKinematic = false;
				// -----------------------------------------------------------------------
				//----- Start of Copypaste from Valve.VR.InteractionSystem.Throwable -----
				// -----------------------------------------------------------------------
				Vector3 position = Vector3.zero;
				Vector3 velocity = Vector3.zero;
				Vector3 angularVelocity = Vector3.zero;
				if (hand == null || hand.controller == null)
				{
					velocity = velocityEstimator.GetVelocityEstimate();
					angularVelocity = velocityEstimator.GetAngularVelocityEstimate();
					position = velocityEstimator.transform.position;
				}
				else
				{
					velocity = Player.instance.trackingOriginTransform.TransformVector(hand.controller.velocity);
					angularVelocity = Player.instance.trackingOriginTransform.TransformVector(hand.controller.angularVelocity);
					position = hand.transform.position;
				}

				Vector3 r = transform.TransformPoint(rigidbody.centerOfMass) - position;
				rigidbody.velocity = velocity + Vector3.Cross(angularVelocity, r);
				rigidbody.angularVelocity = angularVelocity;

				// Make the object travel at the release velocity for the amount
				// of time it will take until the next fixed update, at which
				// point Unity physics will take over
				float timeUntilFixedUpdate = (Time.fixedDeltaTime + Time.fixedTime) - Time.time;
				transform.position += timeUntilFixedUpdate * velocity;
				float angle = Mathf.Rad2Deg * angularVelocity.magnitude;
				Vector3 axis = angularVelocity.normalized;
				transform.rotation *= Quaternion.AngleAxis(angle * timeUntilFixedUpdate, axis);
				// -----------------------------------------------------------------------
				//-----  End of Copypaste from Valve.VR.InteractionSystem.Throwable  -----
				// -----------------------------------------------------------------------

			}

			if (!dissolving)
				StartCoroutine(DissolveRoutine());
			dissolving = true;
		}

		IEnumerator DissolveRoutine ()
		{
			float duration = 2f;
			float timer = duration;
			var originalScale = transform.localScale;
			while (timer > 0)
			{
				timer -= Time.deltaTime;
				transform.localScale = originalScale * (timer / duration);
				yield return null;
			}
			Destroy(gameObject);
		}
	}
}

