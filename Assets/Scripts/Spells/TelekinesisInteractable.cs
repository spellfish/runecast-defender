using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

namespace Spellfish
{

	[RequireComponent(typeof(Crystal))]
	[RequireComponent(typeof(Interactable))]
	public class TelekinesisInteractable : MonoBehaviour
	{
		public UnityEvent onHandHoverBegin;
		public UnityEvent onHandHoverEnd;
		public UnityEvent onHoverAndTriggerDown;
		public UnityEvent onHoverAndTriggerUp;

		Crystal crystal;

		private void Awake ()
		{
			crystal = GetComponent<Crystal>();
		}

		private void OnHandHoverBegin ()
		{
			onHandHoverBegin.Invoke();
		}

		private void OnHandHoverEnd ()
		{
			onHandHoverEnd.Invoke();
			hoverFirstUpdate = false;
		}

		bool hoverFirstUpdate = false;

		private void HandHoverUpdate (Hand hand)
		{
			if (hoverFirstUpdate == false)
			{
				hoverFirstUpdate = true;
				var telekinesis = hand.GetComponent<Telekinesis>();
				telekinesis.Hover(crystal);
			}

			if (hand.controller.GetHairTriggerDown())
			{
				onHoverAndTriggerDown.Invoke();

			}
			else if (hand.controller.GetHairTriggerUp())
			{
				onHoverAndTriggerUp.Invoke();
			}
		}
	}
}
