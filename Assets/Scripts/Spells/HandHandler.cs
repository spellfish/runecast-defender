using UnityEngine;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	/// <summary>
	/// Manages a Valve Hand, so multiple Spells can access its input
	/// </summary>
	public class HandHandler : MonoBehaviour
	{
		public bool button;
		public bool buttonUp;
		public bool trigger;
		public bool triggerDown;
		public bool triggerUp;
		public bool touchpadTouch;
		public Vector2 touchAxis;

		internal Hand hand;


		void Awake ()
		{
			hand = GetComponent<Hand>();
		}

		private void Update ()
		{
			button = buttonUp = trigger = triggerDown = triggerUp = touchpadTouch = false;
			touchAxis = Vector2.zero;

			if(hand.controller != null)
			{
				button = hand.controller.GetPress(Valve.VR.EVRButtonId.k_EButton_Grip);
				buttonUp = hand.controller.GetPressUp(Valve.VR.EVRButtonId.k_EButton_Grip);
				trigger = hand.controller.GetHairTrigger();
				triggerDown = hand.controller.GetHairTriggerDown();
				triggerUp = hand.controller.GetHairTriggerUp();
				touchpadTouch = hand.controller.GetTouch(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
				touchAxis = hand.controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
			}
		}
	}
}
