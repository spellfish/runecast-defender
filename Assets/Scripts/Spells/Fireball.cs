using Valve.VR.InteractionSystem;
using UnityEngine;
using System.Collections.Generic;

namespace Spellfish
{
	/// <summary>
	/// Fireball spell
	/// </summary>
	public class Fireball : MonoBehaviour, ISpell
	{
		public FireballProjectile projectilePrefab;
		public float cooldown = 1.5f;
		public float projectileDelay = 1.5f;
		public int maxProjectileCount = 5;
		public float damagePerProjectile = 10f;
		public float minSpeed = 2f;

		List<IFireballTarget> lockedTargets = new List<IFireballTarget>();
		List<Pair<FireballProjectile, IFireballTarget>> projectile_target_list
			= new List<Pair<FireballProjectile, IFireballTarget>>();

		float projectileDelayTimer;
		float cooldownTimer = 0;
		HandHandler handHandler;
		VelocityEstimator velocityEstimator;

		private void Awake()
		{
			handHandler = GetComponent<HandHandler>();
			velocityEstimator = GetComponent<VelocityEstimator>();

			SpellManager.instance.Register(this);
		}

		void Update()
		{
			if (cooldownTimer > 0)
				cooldownTimer -= Time.deltaTime;

			if (projectileDelayTimer > 0)
				projectileDelayTimer -= Time.deltaTime;

			if (cooldownTimer > 0)
				return;

			if (projectileDelayTimer <= 0 && projectile_target_list.Count < maxProjectileCount
				&& handHandler.button)
			{
				if (projectile_target_list.Count == 0)
					velocityEstimator.BeginEstimatingVelocity();

				var newProjectile = Instantiate(projectilePrefab);
				AlignProjectileWithHand(newProjectile);

				var newTarget = FindTarget();
				var newPair = new Pair<FireballProjectile, IFireballTarget>(newProjectile, newTarget);
				projectile_target_list.Add(newPair);

				if (newTarget != null)
				{
					newTarget.FireballMarker.Show();
					lockedTargets.Add(newTarget);
				}
				handHandler.hand.controller.TriggerHapticPulse(1000);
				projectileDelayTimer = projectileDelay;
			}

			if (projectile_target_list.Count > 0)
			{
				var letgo = handHandler.buttonUp;

				foreach (var pair in projectile_target_list)
				{
					var oldTarget = pair.second;
					AlignProjectileWithHand(pair.first);
					lockedTargets.Remove(oldTarget);

					pair.second = FindTarget();

					if (pair.second != null)
					{
						pair.second.FireballMarker.Show();
						lockedTargets.Add(pair.second);
					}

					if (oldTarget != null && oldTarget != pair.second)
						oldTarget.FireballMarker.Hide();
				}

				if (letgo)
					Throw();
			}
		}

		void Throw ()
		{
			velocityEstimator.FinishEstimatingVelocity();
			var velocityEstimate = velocityEstimator.GetVelocityEstimate();

			for (int i = 0; i < projectile_target_list.Count; i++)
			{
				projectile_target_list[i].first.StartFlying(projectile_target_list[i].second, handHandler.hand, damagePerProjectile);
				projectile_target_list[i].first.speed = velocityEstimate.magnitude;

				if (velocityEstimate.magnitude <= minSpeed)
					projectile_target_list[i].first.Dissolve();

				if (projectile_target_list[i].second != null)
					projectile_target_list[i].second.FireballMarker.Hide();
				if (velocityEstimate.magnitude < minSpeed)
					projectile_target_list[i].first.Dissolve();
			}

			projectile_target_list.Clear();
			lockedTargets.Clear();

			cooldownTimer = cooldown;
			handHandler.hand.controller.TriggerHapticPulse(1000);
		}

		IFireballTarget FindTarget ()
		{
			if (FireballTargetSwitch.Instance != null && FireballTargetSwitch.Instance.enabled)
				return FireballTargetSwitch.Instance;

			List<IFireballTarget> visibleTargets = new List<IFireballTarget>();

			var allTargets = Enemy.All;
			foreach (var target in allTargets)
				if (target != null && target.GazeTracker.isInGaze)
					visibleTargets.Add(target);

			float minDist = float.MaxValue;
			IFireballTarget bestTarget = null;

			foreach (var target in visibleTargets)
				if (target.GazeTracker.gazeDistance < minDist && lockedTargets.Contains(target) == false)
				{
					minDist = target.GazeTracker.gazeDistance;
					bestTarget = target;
				}

			if (bestTarget == null)
			{
				minDist = float.MaxValue;
				foreach (var target in visibleTargets)
					if (target.GazeTracker.gazeDistance < minDist)
					{
						minDist = target.GazeTracker.gazeDistance;
						bestTarget = target;
					}
			}

			return bestTarget;
		}

		void AlignProjectileWithHand (FireballProjectile projectile)
		{
			projectile.transform.parent = transform;
			projectile.transform.localPosition = Vector3.zero;
		}
	}
}
