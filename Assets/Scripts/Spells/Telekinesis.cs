using UnityEngine;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	/// <summary>
	/// Pick up Enemies and throw them around
	/// </summary>
	public class Telekinesis : MonoBehaviour, ISpell
	{
		public AudioClip audioClip;

		[Tooltip("Fixed distance between plyer and grabbed object")]
		public float distanceAfterGrab = 20f;
		public float telekineticMoveSpeed = 20f;

		HandHandler handHandler;
		TelekinesisPointer telekinesisPointer;
		LineRenderer lineRenderer;
		AudioSource audioSource;

		Crystal hoverCrystal, grabbedCrystal;
		CrystalSocket hoverSocket = null;


		void Awake ()
		{
			telekinesisPointer = GetComponentInChildren<TelekinesisPointer>();
			lineRenderer = GetComponentInChildren<LineRenderer>();
			handHandler = GetComponent<HandHandler>();
			audioSource = gameObject.AddComponent<AudioSource>();
			audioSource.playOnAwake = false;
			audioSource.loop = true;
			SpellManager.instance.Register(this);
			enabled = false;
		}

		void Update ()
		{
			lineRenderer.enabled = false;

			if (grabbedCrystal == null)
			{
				audioSource.Stop();
				Point();
			}

			if (grabbedCrystal != null)
			{
				if(!audioSource.isPlaying)
					audioSource.PlayOneShot(audioClip);

				MoveGrabbedObject();

				if (handHandler.triggerUp)
					Drop();
			}
		}

		private void OnDisable()
		{
			Drop();
			lineRenderer.enabled = false;
		}


		internal void Hover(Crystal crystal)
		{
			hoverCrystal = crystal;
		}

		void Point ()
		{
			if(hoverCrystal != null)
			{
				telekinesisPointer.transform.localPosition = hoverCrystal.transform.position;

				if (handHandler.triggerDown)
					Grab(hoverCrystal);
			}
		}


		void MoveGrabbedObject ()
		{
			Ray ray = new Ray(transform.position, telekinesisPointer.transform.forward);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, float.MaxValue, LayerManager.Instance.crystalSocketLayer))
			{
				// Snap to socket if possible
				var socket = hit.transform.gameObject.GetComponent<CrystalSocket>();
				if (hoverSocket != null && socket != hoverSocket)
					hoverSocket.hoverCrystal = null;

				telekinesisPointer.transform.position = hit.transform.position;

				hoverSocket = socket.Hover(grabbedCrystal, handHandler.hand);
			}
			else
			{
				// Otherwise: Follow hand motion
				telekinesisPointer.transform.position = transform.position + telekinesisPointer.transform.forward * distanceAfterGrab; ;

				if (hoverSocket != null)
					hoverSocket = hoverSocket.StopHover();
			}

			var crystalT = grabbedCrystal.transform;
			var targetT = telekinesisPointer.transform;
			var timeDistance = Time.deltaTime * Vector3.Distance(crystalT.position, targetT.position);
			crystalT.position = Vector3.MoveTowards(crystalT.position, targetT.position, telekineticMoveSpeed * timeDistance);
			DrawBeam();
		}

		void DrawBeam ()
		{
			lineRenderer.enabled = true;

			var idealBeam = telekinesisPointer.transform.position - transform.position;
			Debug.DrawLine(transform.position, transform.position + idealBeam);
			var stepLength = idealBeam.magnitude / lineRenderer.numPositions;
			var maxDeltaOffset = grabbedCrystal.transform.position - telekinesisPointer.transform.position; //local
			Debug.DrawLine(telekinesisPointer.transform.position, telekinesisPointer.transform.position + maxDeltaOffset);
			var positions = new Vector3[lineRenderer.numPositions];
			for (int x = 0; x < lineRenderer.numPositions; x++)
			{
				var tempPos = transform.position + idealBeam.normalized * stepLength * x; //global

				var y = Mathf.Pow((x / (float)(lineRenderer.numPositions)), 2);
				var deltaOffset = maxDeltaOffset * y;
				Debug.DrawLine(tempPos, tempPos + deltaOffset);

				positions[x] = tempPos + deltaOffset;
			}

			lineRenderer.SetPositions(positions);
		}


		void Grab (Crystal crystal)
		{
			hoverCrystal = null;
			crystal._animator.SetTrigger("pick up");
			grabbedCrystal = crystal;
			grabbedCrystal.DetachFromSocket();
			grabbedCrystal.transform.rotation = new Quaternion();
			CrystalSocket.SetHighlightForAll(true);
		}

		public void Drop ()
		{
			if (grabbedCrystal != null)
				grabbedCrystal.AttachToSocket(hoverSocket);

			grabbedCrystal = null;
			CrystalSocket.SetHighlightForAll(false);
		}
	}
}

