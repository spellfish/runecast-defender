using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Spellfish
{
	/// <summary>
	/// Anchor point for objects lifted up with telekinsesis
	/// </summary>
	public class TelekinesisPointer : MonoBehaviour
	{
		private void OnDrawGizmos()
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position, transform.position + transform.forward * 20);
		}
	}
}
