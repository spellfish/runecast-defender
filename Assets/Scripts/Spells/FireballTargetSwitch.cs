using UnityEngine;

namespace Spellfish
{
	public class FireballTargetSwitch : MonoBehaviour, IFireballTarget
	{
		public UI.FireballMarker FireballMarker { get; set; }
		public SteamVR_GazeTracker GazeTracker { get; private set; }
		public static FireballTargetSwitch Instance { get; set; }

		private void Awake()
		{
			Instance = this;

			GazeTracker = GetComponent<SteamVR_GazeTracker>();
			FireballMarker = GetComponentInChildren<UI.FireballMarker>(true);
		}

		public void OnFireballHit(float damage)
		{
			MainMenu.Instance.StartGame();
			Destroy(gameObject);
		}
	}
}
