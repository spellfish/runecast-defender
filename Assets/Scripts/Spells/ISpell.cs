using Valve.VR.InteractionSystem;

namespace Spellfish
{
	/// <summary>
	/// A magic spell
	/// </summary>
	public interface ISpell
	{
		bool enabled { get; set; }
	}
}
