using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Targets that may be shot at using the Fireball spell
	/// </summary>
	public interface IFireballTarget
	{
		Transform transform { get; }
		UI.FireballMarker FireballMarker { get; set; }
		SteamVR_GazeTracker GazeTracker { get; }


		void OnFireballHit (float damage);
	}
}
