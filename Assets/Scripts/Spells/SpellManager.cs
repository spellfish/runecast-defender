using System.Collections.Generic;
using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Gives access to all spells
	/// </summary>
	public class SpellManager : MonoBehaviour
	{
		public static SpellManager instance;
		public List<ISpell> spells = new List<ISpell>();


		private void Awake ()
		{
			if (instance == null)
				instance = this;
			else
				Destroy(this);

			var gameGoal = FindObjectOfType<GameGoal>();
			gameGoal.Defeat += GameGoal_Defeat;
		}


		public void Register (ISpell spell)
		{
			spells.Add(spell);
		}

		public void EnableSpells ()
		{
			foreach (var spell in spells)
				spell.enabled = true;
		}

		public void DisableSpells ()
		{
			foreach (var spell in spells)
				spell.enabled = false;
		}


		private void GameGoal_Defeat (object sender, System.EventArgs e)
		{
			DisableSpells();
		}
	}
}
