using System.Collections.Generic;
using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Building that attacks nearby Enemies
	/// </summary>
	public class TowerBuilding : Building
	{
		public BoxCollider detectionZone;
		public BoxCollider damageZone;
		public GameObject[] previewModels;
		public GameObject areaOfEffectPreview;
		public GameObject[] towerModels;
		public GameObject fireGround;

		[Header("Settings")]
		public float energyCostPerSocket;
		[Tooltip("Afterburn")]
		public float totalBurnDamage, totalBurnDuration, burnTickDuration;
		[Tooltip("Damage per second while enemies are in the damage zone")]
		public float fireDamage = 50;
		public float cooldown;
		public float fireGroundDuration = 5f;

		StatBar cooldownBar;
		public List<CrystalSocket> activeSockets = new List<CrystalSocket>();
		public float cooldownTimer = 0;
		public float fireGroundTimer = 0;
		public AudioClip attack;
		public AudioClip spawn;
		public AudioClip despawn;


		AudioSource audioSource;


		override protected void Awake ()
		{
			base.Awake();
			cooldownBar = GetComponentInChildren<StatBar>(true);
			audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.volume = 0.5f;
            audioSource.playOnAwake = false;
		}

		override protected void OnDestroy ()
		{
			base.OnDestroy();
		}

		private void Update ()
		{
			if (cooldownTimer > 0)
			{
				if(cooldownTimer < cooldown / activeSockets.Count)
					cooldownBar.Set(cooldownTimer, cooldown / activeSockets.Count);

				cooldownTimer -= Time.deltaTime;
			}

			if (activeSockets.Count > 0 && cooldownTimer <= 0)
			{
				cooldownBar.gameObject.SetActive(false);
				if (DetectEnemy())
					StartAttack();
				fireGround.SetActive(true);

			}

			if (fireGroundTimer > 0)
			{
				fireGroundTimer -= Time.deltaTime;
				ExecuteAttack();

				cooldownTimer = activeSockets.Count > 0 ? cooldown / activeSockets.Count : cooldown;

				if (fireGroundTimer <= 0)
                {
					fireGround.SetActive(false);
					audioSource.Stop();
                }

            }

        }

		void StartAttack ()
		{
			audioSource.volume = 1;
			audioSource.priority = 0;
			audioSource.PlayOneShot(attack);
			foreach (var socket in activeSockets)
				socket.ConsumeEnergy(energyCostPerSocket);

			fireGroundTimer = fireGroundDuration;

			fireGround.SetActive(true);
			float normalDuration = 3f;
			//fireGround.GetComponent<Animator>().speed = fireGroundTimer / normalDuration;
		}

		void ExecuteAttack ()
		{
			var totalFireDamage = this.totalBurnDamage * activeSockets.Count;
            foreach (var enemy in Enemy.All)
				if (damageZone.bounds.Contains(enemy.transform.position))
				{
					enemy.SetOnFire(totalFireDamage, totalBurnDuration, burnTickDuration);
					enemy.Damage(fireDamage * Time.deltaTime);
				}
		}

		bool DetectEnemy ()
		{
			foreach (var enemy in Enemy.All)
				if (detectionZone.bounds.Contains(enemy.transform.position))
					return true;

			return false;
		}

		void DisableAllVisuals ()
		{
			foreach (var model in towerModels)
				model.SetActive(false);

			foreach (var preview in previewModels)
				preview.SetActive(false);
		}


		protected override void Socket_CrystalEntered (object sender, CrystalSocket.Args e)
		{
			activeSockets.Add(e.crystalsocket);
			audioSource.PlayOneShot(spawn);
			DisableAllVisuals();
			areaOfEffectPreview.SetActive(false);
			towerModels[activeSockets.Count - 1].SetActive(true);
		}

		protected override void Socket_CrystalRemoved (object sender, CrystalSocket.Args e)
		{
			activeSockets.Remove(e.crystalsocket);
			audioSource.PlayOneShot(despawn);
			DisableAllVisuals();
			if (activeSockets.Count > 0)
				towerModels[activeSockets.Count - 1].SetActive(true);
		}

		protected override void Socket_HoverEnd (object sender, CrystalSocket.Args e)
		{
			DisableAllVisuals();
			if (activeSockets.Count > 0)
				towerModels[activeSockets.Count - 1].SetActive(true);

			areaOfEffectPreview.SetActive(false);
		}

		protected override void Socket_HoverStart (object sender, CrystalSocket.Args e)
		{
			if (activeSockets.Count < previewModels.Length)
				previewModels[activeSockets.Count].SetActive(true);

			areaOfEffectPreview.SetActive(true);
		}
	}
}
