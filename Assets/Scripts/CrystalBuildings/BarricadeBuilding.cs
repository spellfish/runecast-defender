using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Building that blocks a pathway, so enemies have to take a different route
	/// </summary>
	public class BarricadeBuilding : Building
	{
		public GameObject NavMeshObstacle;
		public GameObject Preview;
		private bool IsActive;

		public AudioClip barricade_up;
		public AudioClip barricade_down;

		private AudioSource audioSource;

		private CrystalSocket current_socket;
		override protected void Awake ()
		{
			base.Awake();

			audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.volume = 1f;
			audioSource.priority = 100;
			audioSource.playOnAwake = false;
		}

		override protected void OnDestroy ()
		{
			base.OnDestroy();
		}

		private void Update ()
		{
			if (IsActive)
				current_socket.attachedCrystal.ConsumeEnergy(5 * Time.deltaTime);
		}

		protected override void Socket_CrystalEntered (object sender, CrystalSocket.Args e)
		{
			current_socket = sender as CrystalSocket;
			IsActive = true;
			NavMeshObstacle.GetComponent<Animator>().SetTrigger("up");
			audioSource.PlayOneShot(barricade_up);
		}

		protected override void Socket_CrystalRemoved (object sender, CrystalSocket.Args e)
		{
			NavMeshObstacle.GetComponent<Animator>().SetTrigger("down");
			audioSource.PlayOneShot(barricade_down);
			IsActive = false;
		}

		protected override void Socket_HoverEnd (object sender, CrystalSocket.Args e)
		{
			Preview.SetActive(false);
		}

		protected override void Socket_HoverStart (object sender, CrystalSocket.Args e)
		{
			Preview.SetActive(true);
		}
	}
}
