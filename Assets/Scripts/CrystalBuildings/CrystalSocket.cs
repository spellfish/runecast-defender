using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Valve.VR.InteractionSystem;

namespace Spellfish
{
	/// <summary>
	/// Socket in which you can place a Crystal to power a connected Building
	/// </summary>
	public class CrystalSocket : MonoBehaviour
	{
		public event EventHandler<Args> CrystalEntered;
		public event EventHandler<Args> CrystalRemoved;
		public event EventHandler<Args> HoverStart;
		public event EventHandler<Args> HoverEnd;

		public GameObject highlightObject;

		internal Crystal attachedCrystal;
		internal Crystal hoverCrystal = null;
		internal bool hover;
		internal SphereCollider sphereCollider;

		Hand hoverHand;
		bool hoverLastFrame = false;

		public static List<CrystalSocket> _AllSockets;
		public static List<CrystalSocket> AllSockets
		{
			get { if (_AllSockets == null) _AllSockets = new List<CrystalSocket>(); return _AllSockets; }
			private set { _AllSockets = value; }
		}

		public static void SetHighlightForAll (bool active)
		{
			foreach (var socket in AllSockets)
				if (socket.highlightObject != null && socket.attachedCrystal == null && socket.Activated)
					socket.highlightObject.SetActive(active);
		}

		internal bool Activated { get { return enabled; } set { enabled = value; sphereCollider.enabled = value; } }


		void Awake ()
		{
			AllSockets.Add(this);
			sphereCollider = GetComponent<SphereCollider>();
			Activated = false;
		}

		void Update ()
		{
			UpdateHover();
		}

		public void Attach (Crystal crystal)
		{
			highlightObject.SetActive(false);
			HoverEnd(this, new Args(attachedCrystal, this));
			attachedCrystal = crystal;
			var audioSource = GetComponent<AudioSource>();
			if (audioSource != null)
				audioSource.Play();
			attachedCrystal.transform.position = transform.position;
			sphereCollider.enabled = false;
			//TriggerCrystalDestroyAnimation If possible
			if (CrystalEntered != null)
				CrystalEntered(this, new Args(attachedCrystal, this));
		}

		public Crystal Detach ()
		{
			if (CrystalRemoved != null)
				CrystalRemoved(this, new Args(attachedCrystal, this));

			sphereCollider.enabled = true;

			var crystal = attachedCrystal;
			attachedCrystal = null;
			return crystal;
		}

		void UpdateHover ()
		{
			if (hoverCrystal != null)
				hoverCrystal._animator.SetBool("hover", hover);

			if (hover && hoverCrystal == null)
				hover = false;

			if (hover && hoverLastFrame)
				return;

			if (hover && hoverLastFrame == false)
			{
				sphereCollider.radius *= 2;
				hoverHand.controller.TriggerHapticPulse();
				hoverCrystal._animator.SetBool("hover", true);

				if (HoverStart != null)
					HoverStart(this, new Args(attachedCrystal, this));
			}

			if (hover == false && hoverLastFrame == true)
			{
				sphereCollider.radius /= 2;

				if (hoverCrystal != null)
				{
					hoverHand.controller.TriggerHapticPulse();
					hoverCrystal._animator.SetBool("hover", false);
					hoverCrystal = null;
				}

				hoverHand = null;

				if (HoverStart != null)
					HoverEnd(this, new Args(attachedCrystal, this));
			}

			hoverLastFrame = hover;
		}

		internal CrystalSocket Hover (Crystal grabbedCrystal, Hand hand)
		{
			hoverCrystal = grabbedCrystal;
			hoverHand = hand;
			hover = true;

			return this;
		}

		internal CrystalSocket StopHover ()
		{
			hover = false;
			return null;
		}

		public void ConsumeEnergy (float amount)
		{
			if (attachedCrystal != null)
				attachedCrystal.ConsumeEnergy(amount);
		}


		public class Args : EventArgs
		{
			public Crystal crystal { get; set; }

			public CrystalSocket crystalsocket { get; set; }

			public Args (Crystal crystal, CrystalSocket crystalsocket)
			{
				this.crystal = crystal;
				this.crystalsocket = crystalsocket;
			}
		}
	}
}
