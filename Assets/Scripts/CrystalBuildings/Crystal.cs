using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace Spellfish
{
	/// <summary>
	/// Energy crystal that can be placed in a CrystalSocket to power a connected Building
	/// </summary>
	public class Crystal : MonoBehaviour
	{
		public static List<Crystal> _All;
		public static List<Crystal> All
		{
			get { if (_All == null) _All = new List<Crystal>(); return _All; }
			private set { _All = value; }
		}

		public UnityEvent NotEnoughEnergy_Feedback;

        public Type type;

		public AudioClip spawn;
		public AudioClip entered;
		public AudioClip destroy;
		public AudioClip hover;

		private AudioSource audioSource;

		[HideInInspector]
		public MeshRenderer meshRenderer;
		public CrystalSocket socket;

		public int maxEnergy;
		public float energy;
		// for display purposes, is lowered by consume-coroutine
		float energyDisplay;

		[Tooltip("Amount by which the Energy Bar changes per second")]
		public float energySpeed;

		internal Animator _animator;
		StatBar energyBar;

        public Material NotEnoughEnergyMaterial;
        [HideInInspector]
        public Material StandardMaterial;


        bool destroying = false;
		Vector3 spawnLocation;
		Quaternion spawnRotation;
		internal CrystalSpawnPoint spawnPoint;

		public enum Type
		{
			red, yellow, blue, white
		}


		private void Awake ()
		{
			All.Add(this);

            meshRenderer = GetComponentInChildren<MeshRenderer>();
			_animator = GetComponent<Animator>();
			energyBar = GetComponentInChildren<StatBar>(true);
            StandardMaterial = meshRenderer.material;
            energyDisplay = energy = maxEnergy;

			audioSource = gameObject.AddComponent<AudioSource>();
			audioSource.priority = 0;
            audioSource.playOnAwake = false;

			spawnLocation = transform.position;
			spawnRotation = transform.rotation;
		}
		void Start()
		{
			audioSource.PlayOneShot(spawn);
		}

		private void Update ()
		{
			if (energyDisplay != energy)
			{
				//energyDisplay = Mathf.MoveTowards(energyDisplay, energy, energySpeed * Time.deltaTime);
				energyDisplay = Mathf.Lerp(energyDisplay, energy-1, energySpeed * Time.deltaTime);
				energyBar.Set(energyDisplay, maxEnergy);
			}

			energyBar.gameObject.SetActive(energyDisplay < maxEnergy && energyDisplay > 0);

			if (energyDisplay <= 0 && destroying == false)
				ForceDetach();
		}

		private void OnDestroy ()
		{
			All.Remove(this);
		}


		public void AttachToSocket (CrystalSocket socket)
		{
			if (socket == null)
			{
				transform.position = spawnLocation;
				transform.rotation = spawnRotation;
				_animator.SetTrigger("drop");
			}
			else
			{
				audioSource.PlayOneShot(entered);
				socket.hover = false;
				socket.Attach(this);
				this.socket = socket;
				_animator.SetTrigger("attach");
				GetComponent<Collider>().enabled = false;
				spawnPoint.StartRespawnTimer();
			}
		}

		public void DetachFromSocket ()
		{
			if (socket != null)
			{
				socket.Detach();
				socket = null;
				_animator.SetTrigger("detach");
			}
		}

		public void ForceDetach ()
		{
			audioSource.PlayOneShot(destroy);
			_animator.SetTrigger("destroy");
			socket.Detach();
			socket = null;
			destroying = true;
		}

		// called by animation - don't delete!
		public void Destroy ()
		{
			Destroy(gameObject);
		}

		public void ConsumeEnergy (float amount)
		{
			if (energy > 0)
                energy -= amount;
        }

		public void HandHover(bool value)
		{
			_animator.SetBool("hand hover", value);
		}
	}
}
