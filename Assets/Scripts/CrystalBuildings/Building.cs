using UnityEngine;

namespace Spellfish
{
	/// <summary>
	/// Generic building that is connected to and fueled by a crystal socket
	/// </summary>
	abstract public class Building : MonoBehaviour
	{
		public CrystalSocket[] sockets;


		protected virtual void Awake ()
		{
			foreach (var socket in sockets)
				SubscribeToAllSocketEvents(socket);
		}

		protected virtual void OnDestroy ()
		{
			foreach (var socket in sockets)
				DesubscribeFromAllSocketEvents(socket);
		}


		void SubscribeToAllSocketEvents (CrystalSocket socket)
		{
			socket.CrystalEntered += Socket_CrystalEntered;
			socket.CrystalRemoved += Socket_CrystalRemoved;
			socket.HoverStart += Socket_HoverStart;
			socket.HoverEnd += Socket_HoverEnd;
		}

		void DesubscribeFromAllSocketEvents (CrystalSocket socket)
		{
			socket.CrystalEntered -= Socket_CrystalEntered;
			socket.CrystalRemoved -= Socket_CrystalRemoved;
			socket.HoverStart -= Socket_HoverStart;
			socket.HoverEnd -= Socket_HoverEnd;
		}


		abstract protected void Socket_HoverEnd (object sender, CrystalSocket.Args e);

		abstract protected void Socket_HoverStart (object sender, CrystalSocket.Args e);

		abstract protected void Socket_CrystalRemoved (object sender, CrystalSocket.Args e);

		abstract protected void Socket_CrystalEntered (object sender, CrystalSocket.Args e);
	}
}
