using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Spellfish
{
	/// <summary>
	/// Spawnpoint for energy crystals
	/// </summary>
	public class CrystalSpawnPoint : MonoBehaviour
	{
		static List<CrystalSpawnPoint> _AllSpawns = new List<CrystalSpawnPoint>();
		public static List<CrystalSpawnPoint> AllSpawns { get { return _AllSpawns; } }

		[HideInInspector]
		public float respawnDuration;
		public Crystal crystalPrefab;
		public Crystal attachedcrystal;

		StatBar cooldownBar;

		public float respawnTimer;


		private void Awake ()
		{
			_AllSpawns.Add(this);
			respawnDuration = crystalPrefab.maxEnergy / 10;
			cooldownBar = GetComponentInChildren<StatBar>();
			cooldownBar.gameObject.SetActive(false);
		}

		private void Start ()
		{
			Spawn ();
		}

		private void Update ()
		{
			if (attachedcrystal != null)
			{
				if (Vector3.Distance(transform.position, attachedcrystal.transform.position) > 2)
					attachedcrystal = null;
				return;
			}
			else
			{
				if (respawnTimer == respawnDuration)
					cooldownBar.gameObject.SetActive(true);

				if (respawnTimer > 0)
				{
					respawnTimer -= Time.deltaTime;
					cooldownBar.Set(respawnDuration - respawnTimer, respawnDuration);
					if (respawnTimer <= 0)
					{
						Spawn();
						cooldownBar.gameObject.SetActive(false);
					}
				}
			}
		}

		private void Spawn ()
		{
			var crystal = Instantiate(crystalPrefab, transform.position, transform.rotation);
			crystal.transform.position = transform.position;
			attachedcrystal = crystal;
			crystal.spawnPoint = this;
		}


		public void StartRespawnTimer ()
		{
			respawnTimer = respawnDuration;
		}
	}
}
