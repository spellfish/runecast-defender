
using System.Collections;
using System.Collections.Generic;

namespace Spellfish
{
	[System.Serializable]
	public class Array<T> : IEnumerable<T>
	{
		public T this[int key]
		{
			get
			{
				return array[key];
			}
			set
			{
				array[key] = value;
			}
		}

		public T[] array;

		public IEnumerator<T> GetEnumerator ()
		{
			return ((IEnumerable<T>)array).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return ((IEnumerable<T>)array).GetEnumerator();
		}
	}
}
