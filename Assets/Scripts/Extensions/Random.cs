namespace Spellfish
{
	public static class Random
	{
		public static float Range (Pair<float, float> range)
		{
			return UnityEngine.Random.Range(range.first, range.second);
		}

		public static float Range (float min, float max)
		{
			return UnityEngine.Random.Range(min, max);
		}

		public static int Range (Pair<int, int> range)
		{
			return UnityEngine.Random.Range(range.first, range.second);
		}

		public static int Range (int min, int max)
		{
			return UnityEngine.Random.Range(min, max + 1);
		}

		public static T Of<T> (T[] array)
		{
			return array[Range(0, array.Length - 1)];
		}
	}
}
