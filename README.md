# README #

This is the main repository for the Runecast Defender Project which stores the Unity Project. 
The repository root corresponds to the Unity project folder.

### Contribution guidelines ###
There are a few simple rules you should stick to when commiting work

* You can do about anything on local/feature branches
* Never push something you could easily clean up
* Never merge something into develop/master that is not finished/clean
* Ask me or another experienced developer for approval - We'll merge your changes into the main branch when we see fit

There are certain coding guidelines you should adhere to. You can find them in addition to other useful information about our technical workflow in the Technical Design Document in our [**Sharepoint group archive**](https://mediadesignhochschule.sharepoint.com/sites/spellfish/Freigegebene%20Dokumente/Tech/TDD.docx?d=w06edafefc525499b8a7307d05e20d409).


Do you miss any important information on this page?
You can extend the wiki using the [**Markdown**](https://bitbucket.org/tutorials/markdowndemo) language.